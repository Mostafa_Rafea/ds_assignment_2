/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_2;

/**
 *
 * @author Mostafa Rafea
 */

import java.util.Scanner;

public class Assignment_2 {

    /**
     * @param args the command line arguments
     */
    
    public static String dec2Bin(int value) {
        
        String str = "";
        
        if (value == 0) {
            return "";
        }
        
        return (str + (dec2Bin(value / 2)) + value % 2);
        
    }
    
    public static void main(String[] args) {
        
        Scanner input = new Scanner(System.in);
        System.out.print("Enter a decimal number: ");
        int number = input.nextInt();
        System.out.println("Your decimal number in binary format: " + dec2Bin(number));
    }
    
}
